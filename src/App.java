import java.util.ArrayList;
import java.util.ListIterator;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("subtask 1 51.10");

        kiTuLap("Devcamp JAVA exercise");


        System.out.println("subtask 2 51.10");


        chuoiDaoNguoc("word", "drow");


        System.out.println("subtask 3 51.10");


        kiTuXuatHienMotLan("Haha");


        System.out.println("task 51.20");


        array();

        System.out.println("subtask 1 51.30");

        xoaKiTuXuatHienHaiLan("bananas");
        xoaKiTuXuatHienHaiLan("devcamp");

        System.out.println("subtask 7 51.30");

        viTriXuatHienDauTien("Devcamp java",'a');


        System.out.println("subtask  51.40");

        vang();
    }

    public static void kiTuLap(String chuoiBanDau){
        String ChuoiLapLai = "";
        String chuoi = chuoiBanDau.toLowerCase();
        for (int i = 0; i < chuoi.length()-1; i++) {
            for (int j = i+1; j < chuoi.length(); j++) {
                if ( chuoi.charAt(i) == chuoi.charAt(j)) {
                    if(ChuoiLapLai.length() > 0){
                        boolean v = false;
                        int k = 0;
                        while ( k < ChuoiLapLai.length() && v == false)  {
                        if(ChuoiLapLai.charAt(k) == chuoi.charAt(i)) {
                            ChuoiLapLai = ChuoiLapLai.replace(ChuoiLapLai.charAt(k), chuoi.charAt(i));
                            v = true;
                        }else {
                            k++;
                        }
                        }

                        if( v == false ) {
                            ChuoiLapLai = ChuoiLapLai + chuoi.charAt(i);
                        }

                    }else {
                        ChuoiLapLai = ChuoiLapLai + chuoi.charAt(i);
                    }


                    
                }
            }

            
        }
        ChuoiLapLai = ChuoiLapLai.replaceAll(" ", "");
        if(ChuoiLapLai == "") {
            System.out.println("Chuỗi lặp lại là: NO");
        }else {
            System.out.println("Chuỗi lặp lại là: " + ChuoiLapLai);
        }
        
    }


    public static void chuoiDaoNguoc(String chuoi1, String chuoi2) {
        if(chuoi1.length() != chuoi2.length()){
            System.out.println("KO");
        }else{
            boolean v = false;
            int i = 0;
            while (i < chuoi1.length() && v == false) {
                if(chuoi1.charAt(i) != chuoi2.charAt(chuoi1.length()-i-1)){
                    v = true;
                }else{
                    i++;
                }
            }

            if(v == false) {
                System.out.println("OK");
            }else {
                System.out.println("KO");
            }
        }
    }

    public static void kiTuXuatHienMotLan(String chuoiBanDau) {
        String kiTuMotLan = "";
        String chuoi = chuoiBanDau.toLowerCase();
        for (int i = 0; i < chuoi.length(); i++) {
            int j = i+1;
            boolean v = false;
            while(j < chuoi.length() && v == false){
                if(chuoi.charAt(i) == chuoi.charAt(j)){
                    v = true;
                }else{
                    j++;
                }
            }
            if(v == false) {
                kiTuMotLan = kiTuMotLan + chuoi.charAt(i);
            }
        }

        if(kiTuMotLan == "") {
            System.out.println("NO");
        }else if(kiTuMotLan.length() > 1){
            System.out.println("kí tự xuất kiện một lần là: " + kiTuMotLan.charAt(0) );
        }else {
            System.out.println("kí tự xuất kiện một lần là: " + kiTuMotLan );
        }
    }


    public static void array() {
        ArrayList<String> listString = new ArrayList<String>();
        listString.add("xanh");
        listString.add("đỏ");
        listString.add("tím");
        listString.add("vàng");
        listString.add("cam");

        System.out.println("subtask 1 Các phần tử có trong list là: ");
        System.out.println("Các phần tử có trong list là: ");
        System.out.println(listString);
        System.out.println("subtask 3 số phần tử có trong list là: ");


        System.out.println("số phần tử có trong list là: " + listString.size() );

        System.out.println("subtask 4  phần tử thứ 4 có trong list là: ");
        System.out.println(listString.get(3));


        System.out.println("subtask 5 phần cuối cùng có trong list là: ");
        for (int i = 0; i < listString.size(); i++) {
            if(i == listString.size()-1){
                System.out.println(listString.get(i));
            }
     
        }


        System.out.println("subtask 6 xóa phần cuối cùng có trong list là: ");
        for (int i = 0; i < listString.size(); i++) {
            if(i == listString.size()-1){
                listString.remove(i);
                System.out.println("phần tử còn lại là: " + listString);
            }
     
        }

        listString.add("cam");


        System.out.println("subtask 6 Sử dụng forEach ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo: ");
        listString.forEach((n) -> System.out.println(n));


        System.out.println("subtask 7 Sử dụng forEach ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo: ");
        ListIterator<String> iterator = listString.listIterator();
        System.out.println("Các phần tử có trong list là: ");
        while (iterator.hasNext()) {
            System.out.println((String) iterator.next());
        }

        System.out.println("subtask 8 Sử dụng vòng lặp for ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo: ");

        for (int i = 0; i < listString.size(); i++) {
            System.out.println(listString.get(i));
        }

        System.out.println("subtask 9  Thêm một màu sắc vào đầu của ArrayList trên và ghi lại ra terminal ArrayList vừa tạo: ");
        listString.add(0, "hồng");
        System.out.println(listString);


        System.out.println("subtask 10  Sửa màu của phần tử thứ 3 của ArrayList trên thành màu vàng và ghi lại ra terminal ArrayList vừa tạo.: ");

        listString.set(2, "vàng");
        System.out.println(listString);
    }

    public static void xoaKiTuXuatHienHaiLan(String chuoiBanDau) {
        String chuoi = chuoiBanDau.toLowerCase();
        for (int i = 0; i < chuoi.length(); i++) {
            for (int j = i + 1; j < chuoi.length(); j++) {
                if(chuoi.charAt(i) == chuoi.charAt(j)){
                    chuoi = chuoi.substring(0, j) + chuoi.substring(j + 1);
                    j--;
                }
            }
        }

        System.out.println(chuoi);
    }

    public static void viTriXuatHienDauTien(String chuoiBanDau , char kiTu) {
        String chuoi = chuoiBanDau.toLowerCase();
        boolean result = false;
        int i = 0;
        while (i < chuoi.length() && result == false) {
            if(chuoi.charAt(i) == kiTu) {
                System.out.println(i+1);
                result = true;
            }else {
                i++;
            }
        }

    } 


    public static void vang() {
        ArrayList<String> listString = new ArrayList<String>();
        listString.add("xanh");
        listString.add("đỏ");
        listString.add("tím");
        listString.add("vàng");
        listString.add("cam");

        // System.out.println("subtask 3 Kiểm tra ArrayList này nếu chứa màu vàng thì in ra terminal chữ “OK” còn ngược lại in ra chữ “KO”: ");
        boolean result = false;
        int i = 0 ;
        while ( i < listString.size() && result == false) {
            if(listString.get(i) == "vàng") {
                result = true;
            }else {
                i++;
            }
        }
        if ( result == false) {
            System.out.println("KO" + listString);
        }else {
            System.out.println("OK");
        }

    }

}
